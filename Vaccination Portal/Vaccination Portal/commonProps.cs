﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vaccination_Portal
{
    internal class commonProps
    {
        // all common properties 
        public int userId;
        public string uName;
        public int IAge;
        public string mobile;
        public string city;
        public int pinCode;
        public string vcName;
        public int vcId;
        public string govId;
        public string vacStatus;
        public string doneStatus;
        public string dtwoStatus;
        public string password;
        public string vcAddress;
    }
}
