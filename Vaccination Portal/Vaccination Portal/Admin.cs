﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vaccination_Portal.Models;

namespace Vaccination_Portal
{
    internal class Admin : commonProps, ICommonFunctions
    {
        // Creating new User used in the Slot Booking Method 
        public void UserCreating(string mob)
        {
            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                try
                {
                    // fetching details from the user
                    Console.WriteLine("Please enter your Name");
                    uName = Console.ReadLine();
                    Console.WriteLine("Please enter your Age");
                    IAge = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter your govt id no");
                    govId = Console.ReadLine();
                    Console.WriteLine("Please enter your pincode");
                    pinCode = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter your City");
                    city = Console.ReadLine();
                    vacStatus = "NotBooked";
                    var newChildUser = new ChildUser()
                    {
                        UserName = uName,
                        Age = IAge,
                        MobileNo = mob,
                        Pincode = pinCode,
                        City = city,
                        VacStatus = vacStatus,
                        GovId = govId,

                    };

                    covid.ChildUser.Add(newChildUser);
                    covid.SaveChanges();
                    Console.WriteLine("User Created succesfully");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        // Actual Slot Booking and used in the Slot Booking Method
        public void AlternateBooking(int uid = 0)
        {
            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                try
                {
                    if(uid == 0)
                    {
                        Console.WriteLine("Please enter the User Id to Book the slot");
                        uid = int.Parse(Console.ReadLine());
                    }                   
                    userId = uid;
                    Console.WriteLine("Please press 1 for Dose 1");
                    Console.WriteLine("Please press 2 for Dose 2");
                    int dosech = int.Parse(Console.ReadLine());
                    var dosecheck = covid.ChildUser.Find(userId); // Finding the row of the userid
                    pinCode = dosecheck.Pincode;
                    switch (dosech)
                    {
                        case 1:
                            // Slot Booking for Case1
                            if (dosecheck == null) // Userid Validation
                            {
                                Console.WriteLine("-------- Please Enter Valid User Id ----------");
                            }
                            else if (dosecheck.VacStatus == "NotBooked") // Status validation
                            {
                                var d1 = new Dose1Table();
                                int selectedCenter;
                                var centers = covid.VaccineCenters.Where(s => s.Pincode == pinCode).ToList();
                                Console.WriteLine("Please Choose from the below centers and enter the center Id");
                                foreach (var cent in centers)
                                {
                                    Console.WriteLine("Id --" + cent.CenterId + "/ Center Name - " + cent.CenterName);
                                }
                                selectedCenter = int.Parse(Console.ReadLine());
                                doneStatus = "Booked1";
                                d1.DoneStatus = doneStatus;
                                d1.CenterId = selectedCenter;
                                d1.UserId = userId;
                                dosecheck.VacStatus = doneStatus;
                                covid.ChildUser.Update(dosecheck); // Updating the Status
                                covid.Dose1Table.Add(d1); // Adding Appointment to Dose1 Table
                                covid.SaveChanges();
                                Console.WriteLine("-------- Slot Booked -----------");
                            }
                            else
                            {
                                if (dosecheck.VacStatus == "Vaccinated1")  // Status validation
                                {
                                    Console.WriteLine("--------------- Please book second  dose slot -----------");
                                }
                                else
                                {
                                    Console.WriteLine(" ------------- Please Complete your Dose 1 -------");
                                }
                            }

                            break;
                        case 2:
                            var d2 = new Dose2Table();

                            if (dosecheck == null) // Userid Validation
                            {
                                Console.WriteLine("-------- Please Enter Valid User Id ----------");
                            }
                            else if (dosecheck.VacStatus == "Vaccinated1") // Status validation
                            {
                                int selectedCenter2;
                                var center2 = covid.VaccineCenters.Where(s => s.Pincode == pinCode).ToList();
                                Console.WriteLine("Please Choose from the below centers and enter the center Id");
                                foreach (var cent in center2)
                                {
                                    Console.WriteLine("Id --" + cent.CenterId + "/ Center Name - " + cent.CenterName);
                                }
                                selectedCenter2 = int.Parse(Console.ReadLine());
                                doneStatus = "Booked2";
                                d2.DoneStatus = doneStatus;
                                d2.CenterId = selectedCenter2;
                                d2.UserId = userId;
                                dosecheck.VacStatus = doneStatus;
                                covid.ChildUser.Update(dosecheck); // Updating the Status
                                covid.Dose2Table.Add(d2); // Adding Appointment to the Dose2 Table 
                                covid.SaveChanges();
                                Console.WriteLine("----------- Slot Booked ---------");
                            }
                            else
                            {
                                if (dosecheck.VacStatus == "NotBooked")
                                {
                                    Console.WriteLine("----------- Please Book Slot for First Dose -----------");
                                }
                                else if (dosecheck.VacStatus == "Booked1")
                                {
                                    Console.WriteLine("-------------------- Please Complete your First Dose ----------------");
                                }
                                else
                                {
                                    Console.WriteLine("------- You Are Fully Vaccinated  and Not Allowed to book the Slot");
                                }
                            }
                            break;
                        default:
                            Console.WriteLine("Please Choose Correct Option");
                            break;
                    }
                    var userMobile = covid.ChildUser.Where(s => s.UserId == userId).Single();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }


        public void SlotBooking()
        {
            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                try
                {
                    Console.WriteLine("Please Enter the Mobile Number");
                    mobile = Console.ReadLine();
                    var mobcheck = covid.MuserLogin.Find(mobile);
                    if(mobile == null)
                    {
                        Console.WriteLine("Please First Register With your Mobile Number");
                    }
                    else 
                    {
                        Console.WriteLine("Please enter the User Id to Book the slot");
                        userId = int.Parse(Console.ReadLine());
                        var usercheck = covid.ChildUser.Find(userId);
                        if(usercheck == null)
                        {
                            UserCreating(mobile);
                            AlternateBooking();
                        }
                        else
                        {
                            AlternateBooking(userId);
                        }
                    }
                    

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void VaccineCertificate()
        {
            Console.WriteLine("Please enter the User Id");
            userId = int.Parse(Console.ReadLine());
            string cert = "";
            using(CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                var certificate = covid.ChildUser.Where(s => s.UserId == userId).ToList(); // User Table
                var ouutp = covid.Dose1Table.Where(s => s.UserId == userId).ToList(); // Dose 1 Table
                var ouutp2 = covid.Dose2Table.Where(s => s.UserId == userId).ToList(); // Dose 2 Table

                foreach (var check in certificate)
                {
                    cert = check.VacStatus;
                    
                }
                if (cert == "Vaccinated1")
                {
                    Console.WriteLine("---------- Vaccine Certificate ------------");
                    foreach (var check in certificate)
                    {
                        Console.WriteLine($"Congratulations {check.UserName} you have Partially Vaccinated ");
                    }
                    
                    foreach (var check in ouutp)
                    {
                        Console.WriteLine("Dose 1 Vaccinated Time " + check.DoneTime);
                    }
                    Console.WriteLine("------ End of Report -----------");
                }
                else if (cert == "Fully Vaccinated")
                {
                    Console.WriteLine("---------- Vaccine Certificate ------------");
                    foreach (var check in certificate)
                    {
                        Console.WriteLine($"Congratulations {check.UserName} you have Fully Vaccinated ");
                    }
                    
                    foreach (var check in ouutp)
                    {
                        Console.WriteLine("Dose 1 Vaccinated Time " + check.DoneTime);
                    }

                   
                    foreach (var check in ouutp2)
                    {
                        Console.WriteLine("Dose 1 Vaccinated Time " + check.DoneTime);
                    }
                    Console.WriteLine("------ End of Report -----------");
                }
                else
                {
                    Console.WriteLine("Please Schedule a slot to get Vaccinated");
                }
                
            }
        }

        // Vaccine Administered 
        public void VaccineAdmin()
        {
            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                try
                {
                    Console.WriteLine("Please enter the User Id to Vaccinate");
                    userId = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please press 1 for Dose 1");
                    Console.WriteLine("Please press 2 for Dose 2");
                    int dosecheck = int.Parse(Console.ReadLine());
                    DateTime dateTime = DateTime.Now;
                    string date = dateTime.ToString();
                    var updateuser = covid.ChildUser.Find(userId);
                    if (updateuser != null && (updateuser.VacStatus == "Booked1" || updateuser.VacStatus == "Booked2"))
                    {
                                              
                        switch (dosecheck)
                        {
                            case 1:
                                // Vaccinating Dose 1 
                                int check = 0;
                                var dose1  = covid.Dose1Table.Where(s => s.UserId == userId).ToList(); // finding the userid in the dose1 table
                                dose1.ForEach(s => check = s.DoneId);
                                var updatedose1 = covid.Dose1Table.Find(check);
                                if (check != 0 && updatedose1.DoneStatus == "Booked1")
                                {
                                    updatedose1.DoneStatus = "Vaccinated1";
                                    updatedose1.DoneTime = date;
                                    covid.Dose1Table.Update(updatedose1);
                                    updateuser.VacStatus = "Vaccinated1";
                                    covid.ChildUser.Update(updateuser);
                                    covid.SaveChanges();
                                    Console.WriteLine("----------- Successfully Vaccinated First Dose -----------");
                                    
                                }
                                else
                                {
                                    if(updatedose1.DoneStatus == "Vaccinated1")
                                    {
                                        Console.WriteLine("----- Please Book Second Dose Slot");
                                    }
                                    else
                                    {
                                        Console.WriteLine("----------- Please Book Your Vaccine Slot--------------");
                                    }
                                    
                                }
                                break;
                            case 2:
                                // Vaccinating Dose 2
                                int check2 = 0;
                                var dose2 = covid.Dose2Table.Where(s => s.UserId == userId).ToList(); // finding the userid in the dose2 table
                                dose2.ForEach(x => check2 = x.DoneId);
                                var updatedose2 = covid.Dose2Table.Find(check2);
                                if (check2 != 0 && updatedose2.DoneStatus == "Booked2")
                                {
                                        dtwoStatus = "Fully Vaccinated";
                                        updatedose2.DoneStatus = dtwoStatus;
                                        updatedose2.DoneTime = date;
                                        covid.Dose2Table.Update(updatedose2);
                                        updateuser.VacStatus = dtwoStatus;
                                        covid.ChildUser.Update(updateuser);
                                        covid.SaveChanges();
                                        Console.WriteLine("----------- Successfully Vaccinated Second Dose -----------");
                                }
                                else
                                {
                                    if(updatedose2.DoneStatus == "Fully Vaccinated")
                                    {
                                        Console.WriteLine("---- You Are Fully Vaccinated");
                                    }
                                    else
                                    {
                                        Console.WriteLine("----------- Please Book Your Vaccine Slot--------------");
                                    }
                                    
                                }
                                break;
                            default:
                                Console.WriteLine("--- Please choose correct option -----");
                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("--------- No Slots are Booked For Your User Id ----");
                    }
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        //Statustics for Single Dose
        public void SingleDoseUsers()
        {

            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                var total = covid.ChildUser.Where(s => s.VacStatus == "Vaccinated1").ToList();
                Console.WriteLine($" ----- Total No Users Vaccinated with Dose1 are {total.Count} ------ ");

                var total2 = covid.ChildUser.Where(s => s.VacStatus == "NotBooked" && s.VacStatus == "Booked1").ToList();
                Console.WriteLine($"---- Yet to Vaccinate with First Dose are {total2.Count} ---- ");
            }
            
        }
        //Statustics for Double Dose
        public void DoubleDoseUsers()
        {

            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                var total = covid.ChildUser.Where(s => s.VacStatus == "Fully Vaccinated").ToList();
                Console.WriteLine($"----- Total No Users Vaccinated with Fully Vaccinated are {total.Count} -----");

                var total2 = covid.ChildUser.Where(s => s.VacStatus == "Vaccinated1" && s.VacStatus == "Booked2").ToList();
                Console.WriteLine($"---- Yet to Vaccinate with Second Dose are {total2.Count} ------");
            }
        }

        // Adding Vaccination Center
        public void AddVacCenter()
        {
            using(CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                
                Console.WriteLine("Please Enter the Vaccine center Name");
                vcName = Console.ReadLine();
                Console.WriteLine("Please Enter the Vaccine Pincode");
                pinCode = int.Parse(Console.ReadLine());
                Console.WriteLine("Please Enter the Vaccine center Address");
                vcAddress = Console.ReadLine();
                var newVac = new VaccineCenters()
                {
                    CenterName = vcName,
                    Pincode = pinCode,
                    CenterAddress = vcAddress
                };
                covid.VaccineCenters.Add(newVac);
                covid.SaveChanges();
                Console.WriteLine("---------------Vaccine Center Added Successfully------------");
            }
        }
        // Updating Vaccination Center
        public void UpdateVacCenter()
        {
            Console.WriteLine("Please Enter the Vaccination Center Id");
            vcId = int.Parse(Console.ReadLine());
            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                var update = covid.VaccineCenters.Find(vcId);
                Console.WriteLine("Please Enter the Vaccine center Name");
                vcName = Console.ReadLine();
                Console.WriteLine("Please Enter the Vaccine Pincode");
                pinCode = int.Parse(Console.ReadLine());
                Console.WriteLine("Please Enter the Vaccine center Address");
                vcAddress = Console.ReadLine();


                update.CenterName = vcName;
                update.Pincode = pinCode;
                update.CenterAddress = vcAddress;
                
                covid.VaccineCenters.Update(update);
                covid.SaveChanges();
                Console.WriteLine("---------------Vaccine Center Updated Successfully------------");
            }
        }
        // Delete Vaccination Center
        public void DeleteVacCenter()
        {
            Console.WriteLine("Please Enter the Vaccination Center Id");
            vcId = int.Parse(Console.ReadLine());
            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                var update = covid.VaccineCenters.Find(vcId);
                covid.VaccineCenters.Remove(update);
                covid.SaveChanges();
                Console.WriteLine("---------------Vaccine Center Deleted Successfully------------");
            }
        }
    }
}
