﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Vaccination_Portal.Models;

namespace Vaccination_Portal
{
    internal class Users : commonProps, ICommonFunctions
    {
        
        // Hashing The Password with Salt
        private string HashPassword(string password, byte[] salt = null)
        {
            if (salt == null || salt.Length != 16)
            {
                // generate a 128-bit salt using a secure PRNG
                salt = new byte[128 / 8];
                using (var rng = RandomNumberGenerator.Create())
                {
                    rng.GetBytes(salt);
                }
            }

            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
            return $"{hashed}:{Convert.ToBase64String(salt)}";
        }

        //Function to verify the hashed  Password 
        private bool VerifyPassword(string hashedPasswordWithSalt, string passwordToCheck)
        {
            // retrieve both salt and password from 'hashedPasswordWithSalt'
            var passwordAndHash = hashedPasswordWithSalt.Split(':');
            if (passwordAndHash == null || passwordAndHash.Length != 2)
                return false;
            var salt = Convert.FromBase64String(passwordAndHash[1]);
            if (salt == null)
                return false;
            // hash the given password
            var hashOfpasswordToCheck = HashPassword(passwordToCheck, salt);
            // compare both hashes
            if (String.Compare(hashedPasswordWithSalt, hashOfpasswordToCheck) == 0)
            {
                return true;
            }
            return false;
        }

        public void SlotBooking()
        {
            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                try
                {
                    Console.WriteLine("Please enter the User Id to Book the slot");
                    userId = int.Parse(Console.ReadLine()); // Taking userid as input
                    Console.WriteLine("Please press 1 for Dose 1");
                    Console.WriteLine("Please press 2 for Dose 2");
                    int dosech = int.Parse(Console.ReadLine());
                    var dosecheck = covid.ChildUser.Find(userId); // findind userid row
                    pinCode = dosecheck.Pincode;
                    switch (dosech)
                    {
                        case 1:
                            if (dosecheck == null)
                            {
                                Console.WriteLine("-------- Please Enter Valid User Id ----------");
                            }
                            else if (dosecheck.VacStatus == "NotBooked")
                            {
                                var d1 = new Dose1Table();
                                int selectedCenter;
                                var centers = covid.VaccineCenters.Where(s => s.Pincode == pinCode).ToList();
                                Console.WriteLine("Please Choose from the below centers and enter the center Id");
                                foreach (var cent in centers)
                                {
                                    Console.WriteLine("Id --" + cent.CenterId + "/ Center Name - " + cent.CenterName);
                                }
                                selectedCenter = int.Parse(Console.ReadLine());
                                doneStatus = "Booked1";
                                d1.DoneStatus = doneStatus;
                                d1.CenterId = selectedCenter;
                                d1.UserId = userId;
                                dosecheck.VacStatus = doneStatus;
                                covid.ChildUser.Update(dosecheck);
                                covid.Dose1Table.Add(d1);
                                covid.SaveChanges();
                                Console.WriteLine("-------- Slot Booked -----------");
                            }
                            else
                            {
                                if (dosecheck.VacStatus == "Vaccinated1") 
                                {
                                    Console.WriteLine("--------------- Please book second  dose slot -----------");
                                }
                                else
                                {
                                    Console.WriteLine(" ------------- Please Complete your Dose 1 -------");
                                }
                            }
                            
                            break;
                        case 2:
                            var d2 = new Dose2Table();
                            
                            if(dosecheck == null)
                            {
                                Console.WriteLine("-------- Please Enter Valid User Id ----------");
                            }
                            else if (dosecheck.VacStatus == "Vaccinated1")
                            {
                                int selectedCenter2;
                                var center2 = covid.VaccineCenters.Where(s => s.Pincode == pinCode).ToList();
                                Console.WriteLine("Please Choose from the below centers and enter the center Id");
                                foreach (var cent in center2)
                                {
                                    Console.WriteLine("Id --" + cent.CenterId + "/ Center Name - " + cent.CenterName);
                                }
                                selectedCenter2 = int.Parse(Console.ReadLine());
                                doneStatus = "Booked2";
                                d2.DoneStatus = doneStatus;
                                d2.CenterId = selectedCenter2;
                                d2.UserId = userId;
                                dosecheck.VacStatus = doneStatus;
                                covid.ChildUser.Update(dosecheck);
                                covid.Dose2Table.Add(d2);
                                covid.SaveChanges();
                                Console.WriteLine("----------- Slot Booked ---------");
                            }
                            else
                            {
                                if (dosecheck.VacStatus == "NotBooked")
                                {
                                    Console.WriteLine("----------- Please Book Slot for First Dose -----------");
                                }
                                else if(dosecheck.VacStatus == "Booked1")
                                {
                                    Console.WriteLine("-------------------- Please Complete your First Dose ----------------");
                                }
                                else
                                {
                                    Console.WriteLine("------- You Are Fully Vaccinated  and Not Allowed to book the Slot");
                                }
                            }
                            break;
                        default:
                            Console.WriteLine("Please Choose Correct Option");
                            break;
                    }
                    var userMobile = covid.ChildUser.Where(s => s.UserId == userId).Single();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void VaccineCertificate()
        {
            Console.WriteLine("Please enter the User Id");
            userId = int.Parse(Console.ReadLine());
            string cert = "";
            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                var certificate = covid.ChildUser.Where(s => s.UserId == userId).ToList();
                var ouutp = covid.Dose1Table.Where(s => s.UserId == userId).ToList();
                var ouutp2 = covid.Dose2Table.Where(s => s.UserId == userId).ToList();

                foreach (var check in certificate)
                {
                    cert = check.VacStatus;

                }
                if (cert == "Vaccinated1")
                {
                    Console.WriteLine("---------- Vaccine Certificate ------------");
                    foreach (var check in certificate)
                    {
                        Console.WriteLine($"---- Congratulations {check.UserName} you have Partially Vaccinated ------");
                    }

                    foreach (var check in ouutp)
                    {
                        Console.WriteLine(" ----- Dose 1 Vaccinated Time " + check.DoneTime + " -----------------");
                    }
                    Console.WriteLine("------ End of Report -----------");
                }
                else if (cert == "Fully Vaccinated")
                {
                    Console.WriteLine("---------- Vaccine Certificate ------------");
                    foreach (var check in certificate)
                    {
                        Console.WriteLine($"Congratulations {check.UserName} you have Fully Vaccinated ");
                    }

                    foreach (var check in ouutp)
                    {
                        Console.WriteLine("Dose 1 Vaccinated Time " + check.DoneTime);
                    }


                    foreach (var check in ouutp2)
                    {
                        Console.WriteLine("Dose 1 Vaccinated Time " + check.DoneTime);
                    }
                    Console.WriteLine("------ End of Report -----------");
                }
                else
                {
                    Console.WriteLine("Please Schedule a slot to get Vaccinated");
                }

            }
        }


        public void MainUserRegister()
        {
            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                try
                {
                    Console.WriteLine("Please enter your 10 digit Mobile No");
                    mobile = Console.ReadLine();
                    var check = covid.MuserLogin.Find(mobile);
                    var newMuser = new MuserLogin();
                    if (check != null) // checking if the user is already registered or not
                    {
                        Console.WriteLine("User Already Registered");
                    }
                    else if (mobile.Length == 10) // checking the length of mobile number
                    { 
                        Console.WriteLine("Please enter your Password");
                        password = Console.ReadLine();
                        string hashedPass = HashPassword(password);

                        newMuser.MobileNo = mobile;
                        newMuser.Passwd = hashedPass;
                        
                        covid.MuserLogin.Add(newMuser); // adding user to login table
                        covid.SaveChanges(); // saving changes
                        Console.WriteLine("User registered succesfully");
                    }
                    else
                    {
                        Console.WriteLine("please enter valid mobile no");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void CreateNewUser(string mob)
        {
            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                try
                {

                    // fetching details from the user
                    Console.WriteLine("Please enter your Name");
                    uName = Console.ReadLine();
                    Console.WriteLine("Please enter your Age");
                    IAge = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter your govt id no");
                    govId = Console.ReadLine();
                    Console.WriteLine("Please enter your pincode");
                    pinCode = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter your City");
                    city = Console.ReadLine();
                    vacStatus = "NotBooked";
                    var newChildUser = new ChildUser()
                    {
                        UserName = uName,
                        Age = IAge,
                        MobileNo = mob,
                        Pincode = pinCode,
                        City = city,
                        VacStatus = vacStatus,
                        GovId = govId,
                        
                    };

                    covid.ChildUser.Add(newChildUser);
                    covid.SaveChanges();
                    Console.WriteLine("User Created succesfully");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void DeletingUser(string mob)
        {
            Console.WriteLine("Please enter the UserID");
            userId = int.Parse(Console.ReadLine());
            using(CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                var del = covid.ChildUser.Find(userId);
                if (del.VacStatus == "NotBooked" && del.MobileNo == mob)
                {
                    covid.ChildUser.Remove(del);
                    covid.SaveChanges();
                    Console.WriteLine("User Deleted Succuessfully");
                }
                else if(del.VacStatus == "Booked1" && del.MobileNo == mob)
                {
                    var del1 = covid.Dose1Table.Find(userId);
                    covid.Dose1Table.Remove(del1);
                    covid.ChildUser.Remove(del);
                    Console.WriteLine("User Deleted Succuessfully and removed the Scheduled Vaccine Slot");
                }
                else
                {
                    Console.WriteLine("You Cannot Delete the User");
                }
            }
        }

        // Transfer of User From One Main User to Other Main User
        public void TransferUser()
        {
            Console.WriteLine("Please enter the UserID");
            userId = int.Parse(Console.ReadLine());
            using (CovidVaccinationContext covid = new CovidVaccinationContext())
            {
                var transfer = covid.ChildUser.Find(userId);
                Console.WriteLine("Please Enter the Mobile No You Want to Transfer");
                mobile = Console.ReadLine();
                var mobCheck = covid.MuserLogin.Find(mobile);
                if(mobCheck.MobileNo == mobile)
                {
                    transfer.MobileNo = mobile;
                    covid.ChildUser.Update(transfer);
                    covid.SaveChanges();
                }
                else
                {
                    Console.WriteLine("The Number entered by you is not Registered.Please Register with given Number");
                }
            }
        }



    }
}
