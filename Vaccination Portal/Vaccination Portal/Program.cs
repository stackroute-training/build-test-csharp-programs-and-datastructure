﻿using System;
using Vaccination_Portal.Models;
using System.Reflection;

namespace Vaccination_Portal
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------- Welecome to Covid Vaccination Portal --------------");
            string input = "Y";
            Users users = new Users(); 
            Admin admin = new Admin();
            while (input.ToLower() == "y")
            {
                Console.WriteLine("Press 1 for User/Admin Login");
                Console.WriteLine("Press 2 for New User Registration");
                int val = int.Parse(Console.ReadLine());

                switch (val)
                {
                    case 1:
                        Console.WriteLine("Press 1 for User Login");
                        Console.WriteLine("Press 2 for Admin Login");
                        int val2 = int.Parse(Console.ReadLine());
                        switch (val2)
                        {
                            case 1:
                                // User Logging 
                                bool logincheck = false;
                                string mob = null; 
                                Type t = users.GetType();
                                using (CovidVaccinationContext covid = new CovidVaccinationContext())
                                {
                                    Console.WriteLine("Please Enter the Mobile Number");
                                    mob = Console.ReadLine();
                                    if (mob.Length == 10) // Checking Mobile no Length
                                    {
                                        var usercheck = covid.MuserLogin.Find(mob);
                                        if(usercheck != null)
                                        {
                                            Console.WriteLine("Please Enter the Password");
                                            string pass = Console.ReadLine();

                                            string hashedPass = usercheck.Passwd; //getting hashed password
                                            // Checking Hashed password with Original Password
                                            var hashCheck = (bool)t.InvokeMember("VerifyPassword", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.InvokeMethod, null, users, new object[] { hashedPass, pass });
                                            if (usercheck.MobileNo == mob && hashCheck)
                                            {
                                                logincheck = true;
                                                Console.WriteLine("---------- Login Successfull ------------");
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("------  Mobile Number Not Registered ----------");
                                        }                                      
                                    }
                                    else
                                    {
                                        Console.WriteLine("----------- Please Enter Valid Mobile Number -----------");
                                    }
                                    
                                }
                                while (logincheck)
                                {
                                    
                                    Console.WriteLine("Press 1 for Creating a user");
                                    Console.WriteLine("Press 2 for Vaccine Slot Booking");
                                    Console.WriteLine("Press 3 for Vaccine Certificate");
                                    Console.WriteLine("Press 4 for delete a user");
                                    Console.WriteLine("Press 5 for Transfer user to another number");
                                    Console.WriteLine("Press 6  to Log Out ");
                                    int val3 = int.Parse(Console.ReadLine());
                                    switch (val3)
                                    {
                                        case 1:
                                            users.CreateNewUser(mob);
                                            break;
                                        case 2:
                                            users.SlotBooking();
                                            break;
                                        case 3:
                                            users.VaccineCertificate();
                                            break;
                                        case 4:
                                            users.DeletingUser(mob);
                                            break;
                                        case 5:
                                            users.TransferUser();
                                            break;
                                        case 6:
                                            //Logut exit from while loop
                                            logincheck = false;
                                            Console.WriteLine("Logged Out Succesfully");
                                            break;
                                        default:
                                            Console.WriteLine("Please Choose correct option");
                                            break;
                                    }

                                }
                                break;
                            case 2:
                                bool admincheck = true;
                                int adminPass = 1; //Default User Login
                                if (adminPass == 1)
                                {
                                    Console.WriteLine("Login Successfull");
                                    while (admincheck)
                                    {
                                        
                                        Console.WriteLine("Press 1 for Creating a user and Slot Booking");
                                        Console.WriteLine("Press 2 for Single Dose Vaccinated Users Statistics");
                                        Console.WriteLine("Press 3 for Vaccine Certificate");
                                        Console.WriteLine("Press 4 for fuly vaccinated status Statistics");
                                        Console.WriteLine("Press 5 for Adding Vaccine Center");
                                        Console.WriteLine("Press 6 for Updating Vaccine Center");
                                        Console.WriteLine("Press 7 for Deleting Vaccine Center");
                                        Console.WriteLine("Press 8 for adminstring the dose for booked user");
                                        Console.WriteLine("Press 9  to Log Out ");
                                        int val3 = int.Parse(Console.ReadLine());
                                        switch (val3)
                                        {
                                            case 1:
                                                admin.SlotBooking();
                                                break;
                                            case 2:
                                                admin.SingleDoseUsers();
                                                break;
                                            case 3:
                                                admin.VaccineCertificate();
                                                break;
                                            case 4:
                                                admin.DoubleDoseUsers();
                                                break;
                                            case 5:
                                                admin.AddVacCenter();
                                                break;
                                            case 6:
                                                admin.UpdateVacCenter();
                                                break;
                                            case 7:
                                                admin.DeleteVacCenter();    
                                                break;
                                            case 8:
                                                admin.VaccineAdmin();
                                                break;
                                            case 9: 
                                                //Logut exit from while loop
                                                admincheck = false;
                                                Console.WriteLine("Logged Out Succesfully");
                                                break;
                                            default:
                                                Console.WriteLine("Please Choose correct option");
                                                break;
                                        }
                                    }
                                }

                                break;
                            default:
                                Console.WriteLine("Please choose correct option");
                                break;
                        }
                        break;
                    case 2:
                        users.MainUserRegister();
                        break;
                    default:
                        Console.WriteLine("please choose correct option");
                        break;
                }
                Console.WriteLine("If You Want to Continue Press Y/N");
                input = Console.ReadLine();
            }

            Console.WriteLine("Thank You for using our Services!!!");
        }
    }
}
