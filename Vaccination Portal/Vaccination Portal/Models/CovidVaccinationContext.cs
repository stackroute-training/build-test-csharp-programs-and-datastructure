﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Vaccination_Portal.Models
{
    public partial class CovidVaccinationContext : DbContext
    {
        public CovidVaccinationContext()
        {
        }

        public CovidVaccinationContext(DbContextOptions<CovidVaccinationContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ChildUser> ChildUser { get; set; }
        public virtual DbSet<Dose1Table> Dose1Table { get; set; }
        public virtual DbSet<Dose2Table> Dose2Table { get; set; }
        public virtual DbSet<MuserLogin> MuserLogin { get; set; }
        public virtual DbSet<VaccineCenters> VaccineCenters { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=localhost;Database=CovidVaccination;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChildUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK__ChildUse__CB9A1CFF2A12E8AE");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.Property(e => e.Age).HasColumnName("age");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnName("city")
                    .HasMaxLength(50);

                entity.Property(e => e.GovId)
                    .HasColumnName("Gov_Id")
                    .HasMaxLength(50);

                entity.Property(e => e.MobileNo)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Pincode).HasColumnName("pincode");

                entity.Property(e => e.UserCreated)
                    .HasColumnName("userCreated")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("userName")
                    .HasMaxLength(50);

                entity.Property(e => e.VacStatus)
                    .IsRequired()
                    .HasColumnName("Vac_status")
                    .HasMaxLength(50);

                entity.HasOne(d => d.MobileNoNavigation)
                    .WithMany(p => p.ChildUser)
                    .HasForeignKey(d => d.MobileNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("userid_mobileno");
            });

            modelBuilder.Entity<Dose1Table>(entity =>
            {
                entity.HasKey(e => e.DoneId)
                    .HasName("PK__dose1Tab__8E06D75E02FD62D8");

                entity.ToTable("dose1Table");

                entity.Property(e => e.DoneId).HasColumnName("done_id");

                entity.Property(e => e.CenterId).HasColumnName("center_id");

                entity.Property(e => e.DoneStatus)
                    .HasColumnName("done_status")
                    .HasMaxLength(50);

                entity.Property(e => e.DoneTime)
                    .HasColumnName("done_time")
                    .HasMaxLength(200);

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Dose1Table)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("userid_done");
            });

            modelBuilder.Entity<Dose2Table>(entity =>
            {
                entity.HasKey(e => e.DoneId)
                    .HasName("PK__dose2Tab__8E06D75E2B24D94F");

                entity.ToTable("dose2Table");

                entity.Property(e => e.DoneId).HasColumnName("done_id");

                entity.Property(e => e.CenterId).HasColumnName("center_id");

                entity.Property(e => e.DoneStatus)
                    .HasColumnName("done_status")
                    .HasMaxLength(50);

                entity.Property(e => e.DoneTime)
                    .HasColumnName("done_time")
                    .HasMaxLength(200);

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Dose2Table)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("userid_dtwo");
            });

            modelBuilder.Entity<MuserLogin>(entity =>
            {
                entity.HasKey(e => e.MobileNo)
                    .HasName("PK__MuserLog__D6D73A873761187E");

                entity.Property(e => e.MobileNo).HasMaxLength(50);

                entity.Property(e => e.Passwd)
                    .IsRequired()
                    .HasMaxLength(300);
            });

            modelBuilder.Entity<VaccineCenters>(entity =>
            {
                entity.HasKey(e => e.CenterId)
                    .HasName("PK__vaccineC__290A28870154EB87");

                entity.ToTable("vaccineCenters");

                entity.Property(e => e.CenterId).HasColumnName("center_id");

                entity.Property(e => e.CenterAddress)
                    .HasColumnName("centerAddress")
                    .HasMaxLength(100);

                entity.Property(e => e.CenterName)
                    .IsRequired()
                    .HasColumnName("centerName")
                    .HasMaxLength(50);

                entity.Property(e => e.Pincode).HasColumnName("pincode");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
