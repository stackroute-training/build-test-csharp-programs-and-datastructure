﻿using System;
using System.Collections.Generic;

namespace Vaccination_Portal.Models
{
    public partial class VaccineCenters
    {
        public int? Pincode { get; set; }
        public int CenterId { get; set; }
        public string CenterName { get; set; }
        public string CenterAddress { get; set; }
    }
}
