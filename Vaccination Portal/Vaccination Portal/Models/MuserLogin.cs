﻿using System;
using System.Collections.Generic;

namespace Vaccination_Portal.Models
{
    public partial class MuserLogin
    {
        public MuserLogin()
        {
            ChildUser = new HashSet<ChildUser>();
        }

        public string MobileNo { get; set; }
        public string Passwd { get; set; }

        public virtual ICollection<ChildUser> ChildUser { get; set; }
    }
}
