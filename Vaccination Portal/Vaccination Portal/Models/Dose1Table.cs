﻿using System;
using System.Collections.Generic;

namespace Vaccination_Portal.Models
{
    public partial class Dose1Table
    {
        public int DoneId { get; set; }
        public int? CenterId { get; set; }
        public string DoneStatus { get; set; }
        public string DoneTime { get; set; }
        public int UserId { get; set; }

        public virtual ChildUser User { get; set; }
    }
}
