﻿using System;
using System.Collections.Generic;

namespace Vaccination_Portal.Models
{
    public partial class ChildUser
    {
        public ChildUser()
        {
            Dose1Table = new HashSet<Dose1Table>();
            Dose2Table = new HashSet<Dose2Table>();
        }

        public int UserId { get; set; }
        public string MobileNo { get; set; }
        public string UserName { get; set; }
        public int Age { get; set; }
        public string VacStatus { get; set; }
        public string GovId { get; set; }
        public int Pincode { get; set; }
        public string City { get; set; }
        public DateTime? UserCreated { get; set; }

        public virtual MuserLogin MobileNoNavigation { get; set; }
        public virtual ICollection<Dose1Table> Dose1Table { get; set; }
        public virtual ICollection<Dose2Table> Dose2Table { get; set; }
    }
}
