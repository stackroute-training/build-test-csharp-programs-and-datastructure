
## Build Test CSharp Programs And Data Structure

### Instructions
Refer the [PROBLEM.md](./PROBLEM.md) file for problem description.

#### To use this as a boilerplate for your Assessment, please follow these steps

1. **FORK** this repository in your Gitlab account

2. **CLONE** the forked repository in your local machine

3. Navigate to `build-test-csharp-programs-and-datastructure` folder

    ` cd build-test-csharp-programs-and-datastructure`

4. Check the status of your repository

     `git status`

5. Complete the solution as per the instructions given in problem.md

6. Use the following command to update the index using the current content found in the working tree, to prepare the content staged for the next commit.

     `git add .`

7. Commit and Push the project to git

     `git commit -a -m "Initial commit | or place your comments according to your need"`

     `git push -u origin master`

8. Check on the git repo online, if the files have been pushed

9. Submit your solution to your mentor


### Important instructions for Participants
> - We expect you to complete this Assessment on your own within the given time.
> - The code must be properly indented and properly commented
> - Follow through the problem statement given in [PROBLEM.md](./PROBLEM.md) file

### MENTORS TO BEGIN REVIEW YOUR WORK ONLY AFTER ->
> - You complete the Assessment and commit in gitlab within the given time.
> - You add the respective Mentor as a Reporter/Master into your Assessment Repository
> - Intimate your Mentor on Slack and/or Send an Email to learner.support@stackroute.in - with your Git URL - when it is ready for final submission.



